# CaosDB WebUI 2 - Module for Info Service

This module contains the JavaScript implementation of the CaosDB Info API.

See [docs.indiscale.com](https://docs.indiscale.com) for more information about the CaosDB Project.

## Generate the WebGRPC Code

* clone/pull git repo `git@gitlab.indiscale.com:caosdb/src/caosdb-proto` (or init/update the submodule).
* Run `./generate_sources.sh`

## Copyright

Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>

(unless stated otherwise in the files)

## Licence

AGPL-3.0-or-later

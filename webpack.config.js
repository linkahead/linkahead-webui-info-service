const path = require('path');

module.exports = {
  entry: path.resolve(__dirname, './src/index.js'),
  resolve: {
    extensions: ['*', '.js', '.js'],
  },
  module: {
    rules: [
      {
        test: /\.(js|js)$/,
        exclude: /node_modules/,
        use: ['babel-loader']
      }
    ]
  },
  output: {
    path: path.resolve(__dirname, './dist'),
    filename: 'caosdb-webui-info-service.js',
    library: 'CaosDBInfoService',
    libraryTarget: 'umd',
  },
  devServer: {
    contentBase: path.resolve(__dirname, './dist'),
  },
};

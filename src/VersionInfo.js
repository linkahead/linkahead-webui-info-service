export class VersionInfo {
  constructor({major, minor, patch, preRelease, build}) {
    this.major = major;
    this.minor = minor;
    this.patch = patch;
    this.preRelease = preRelease;
    this.build = build;
  }

  noBuild() {
    var ret = new VersionInfo(this);
    ret.build = undefined;
    return ret;
  }

  noPreRelease() {
    var ret = new VersionInfo(this);
    ret.preRelease = undefined;
    return ret;
  }

  toString() {
    var ret = `${this.major}.${this.minor}.${this.patch}`;
    if(this.preRelease) {
      ret += `-${this.preRelease}`;
    }
    if(this.build) {
      ret += `+${this.build}`;
    }
    return ret;
  }
}

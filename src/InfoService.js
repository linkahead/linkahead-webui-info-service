import { api } from "./InfoApi";
import { VersionInfo } from "./VersionInfo";
import { SessionInfo } from "./SessionInfo";

export class InfoService {
  constructor(uri) {
    this.uri = uri || "/api";
  }

  async getVersionInfo() {
    const client = new api.v1.GeneralInfoServicePromiseClient(this.uri, null, null);
    const request = new api.v1.GetVersionInfoRequest();

    const response = await client.getVersionInfo(request, {});
    return new VersionInfo(response.getVersionInfo().toObject());
  }

  async _getSessionInfo(username, password) {
    var headers = {};
    if(username) {
      headers["authentication"] = "Basic " + btoa(`${username}:${password}`);
    }

    const client = new api.v1.GeneralInfoServicePromiseClient(this.uri, null, null);
    const request = new api.v1.GetSessionInfoRequest();

    const response = await client.getSessionInfo(request, headers);
    return new SessionInfo({...response.toObject(),
      roles: response.getRolesList(),
      permissions: response.getPermissionsList()});
  }

  /**
   * Request and return the session info of the current session.
   */
  async getSessionInfo() {
    return await this._getSessionInfo();
  }

  /**
   * Login with user name and password and return the session info of the
   * new session.
   */
  async login(password, username) {
    return await this._getSessionInfo(password, username);
  }

  /**
   * Logout. The logout will silently return if the session was not valid in
   * the first place.
   *
   * Throws an error if anything goes wrong.
   */
  async logout() {
    try {
      // these aren't special credentials ("logout", ""). The server will just
      // not accept them and delete the session cookie which is effectively a
      // logout when this happens in a browser.
      const response = await this._getSessionInfo("logout", ""); 

      throw new Error("Logout did not proceed correctly. Something went wrong and it looks like a programming error in the webui. Please report");
    } catch (error) {
      // This is the special GRPC code for unauthenticated requests.
      var UNAUTHENTICATED = 16;
      if(error.code === UNAUTHENTICATED) {
        return true;
      }
      throw error;
    }
  }
}

export class SessionInfo {
  constructor({realm, username, roles, permissions, expires}) {
    this.realm = realm;
    this.username = username;
    this.roles = roles || [];
    this.permissions = permissions || [];
    this.expires = expires;
  }

  /**
   * Return true iff the permissions of this session info imply the given
   * permission.
   */
  implies(permission) {
    if (!this.permissions) {
      return false;
    }

    // e.g. permission is "ACM:ROLE:INSERT"
    const fragments = permission.split(":");
    // ... then fragments is ["ACM", "ROLE", "INSERT"]

    const wild_cards = ["*", permission];
    for (let i = 1; i < fragments.length; i++) {
      wild_cards.push(fragments.slice(0,i).join(":") + "*");
    }
    // ... then wild_cards is ["*", "ACM:*", "ACM:ROLE:*", "ACM:ROLE:INSERT"]


    // now check if in this.permissions is at least one exact match for one of
    // the strings in wild_cards.
    // e.g. assume this.permissions is ["ACM:*", "bla", "bli", "blub"], then we
    // have a match!
    return this.permissions.filter(p => wild_cards.indexOf(p) > -1).length > 0;
  }
}
